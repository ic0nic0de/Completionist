scriptname Ic0n_Completionist_Quests extends quest

Ic0n_Completionist_MCM property MCM auto
Ic0n_Completionist_Arrays property Array auto

;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function addQuestData(bool _RadiantQuests, int firstStage, int lastStage, string editorID, string questName, string _QuestGiver, string _QuestOverview, string _QuestNotes = "")
		
	Array._Array_Quest_Radiant[Array.OptionData] = _RadiantQuests
	Array._Array_Stage_First[Array.OptionData] = firstStage
	Array._Array_Stage_Final[Array.OptionData] = lastStage			
	Array._Array_Quest_ID[Array.OptionData] = editorID
	Array._Array_Quest_Name[Array.OptionData] = questName
	Array._Array_Quest_Giver[Array.OptionData] = _QuestGiver
	Array._Array_Quest_Overview[Array.OptionData] = _QuestOverview
	Array._Array_Quest_Notes[Array.OptionData] = _QuestNotes
	
	Array.OptionData += 1
endfunction
	
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Function _Build_Quests(string mcmpage)

	Int Index
	Int Lngth
	
	if mcmpage == "Main Quests" ;--------------- Skyrim Main Quests			

		Index = 0
		While Index < Array.Main_ID.length
			addQuestData(Array.Main_Radiant[Index], Array.Main_First[Index], Array.Main_Final[Index], Array.Main_ID[Index], Array.Main_Name[Index], Array.Main_Giver[Index], Array.Main_Overview[Index], Array.Main_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Main, Array._Array_Toggle_Main, "Main", Array._Array_Manual_Main)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Main Quests (CW)" ;--------------- Civil War Quests	
		
		Index = 0
		Lngth = Array.MainCW_ID.length
		While Index < Lngth
		
			if Index < 14 && MCM.CW_Faction_Choice == 1
				Lngth = 14
				addQuestData(Array.MainCW_Radiant[Index], Array.MainCW_First[Index], Array.MainCW_Final[Index], Array.MainCW_ID[Index], Array.MainCW_Name[Index], Array.MainCW_Giver[Index], Array.MainCW_Overview[Index], Array.MainCW_Notes[Index])

			elseif Index >= 14 && MCM.CW_Faction_Choice == 2
				addQuestData(Array.MainCW_Radiant[Index], Array.MainCW_First[Index], Array.MainCW_Final[Index], Array.MainCW_ID[Index], Array.MainCW_Name[Index], Array.MainCW_Giver[Index], Array.MainCW_Overview[Index], Array.MainCW_Notes[Index])
	
			elseif MCM.CW_Faction_Choice == 0
				Index = Lngth
				addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
			endif
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_CWMain, Array._Array_Toggle_CWMain, "", Array._Array_Manual_CWMain)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Main Quests (DG)" ;--------------- Dawnguard Main Quests			

		Index = 0
		While Index < Array.MainDG_ID.length
			if Index == 3
				if MCM.DG_Faction_Choice == 1
					addQuestData(Array.MainDG_Radiant[3], Array.MainDG_First[3], Array.MainDG_Final[3], Array.MainDG_ID[3], Array.MainDG_Name[3], Array.MainDG_Giver[3], Array.MainDG_Overview[3], Array.MainDG_Notes[3])
					addQuestData(Array.MainDG_Radiant[4], Array.MainDG_First[4], Array.MainDG_Final[4], Array.MainDG_ID[4], Array.MainDG_Name[4], Array.MainDG_Giver[4], Array.MainDG_Overview[4], Array.MainDG_Notes[4])
					Index = 6
				elseif MCM.DG_Faction_Choice == 2
					addQuestData(Array.MainDG_Radiant[5], Array.MainDG_First[5], Array.MainDG_Final[5], Array.MainDG_ID[5], Array.MainDG_Name[5], Array.MainDG_Giver[5], Array.MainDG_Overview[5], Array.MainDG_Notes[5])
					addQuestData(Array.MainDG_Radiant[6], Array.MainDG_First[6], Array.MainDG_Final[6], Array.MainDG_ID[6], Array.MainDG_Name[6], Array.MainDG_Giver[6], Array.MainDG_Overview[6], Array.MainDG_Notes[6])
					Index = 6
				else
					addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
					addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
					Index = 6
				endif
			else
				addQuestData(Array.MainDG_Radiant[Index], Array.MainDG_First[Index], Array.MainDG_Final[Index], Array.MainDG_ID[Index], Array.MainDG_Name[Index], Array.MainDG_Giver[Index], Array.MainDG_Overview[Index], Array.MainDG_Notes[Index])
			endif
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_DGMain, Array._Array_Toggle_DGMain, "Main", Array._Array_Manual_DGMain)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Main Quests (DB)" ;--------------- Dragonborn Main Quests		
		Index = 0 
		While Index < Array.MainDB_ID.length
			addQuestData(Array.MainDB_Radiant[Index], Array.MainDB_First[Index], Array.MainDB_Final[Index], Array.MainDB_ID[Index], Array.MainDB_Name[Index], Array.MainDB_Giver[Index], Array.MainDB_Overview[Index], Array.MainDB_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_DBMain, Array._Array_Toggle_DBMain, "Main", Array._Array_Manual_DBMain)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Whiterun" ;--------------- Whiterun Main Quests		
		Index = 0
		While Index < Array.MainWhiterun_ID.length
			addQuestData(Array.MainWhiterun_Radiant[Index], Array.MainWhiterun_First[Index], Array.MainWhiterun_Final[Index], Array.MainWhiterun_ID[Index], Array.MainWhiterun_Name[Index], Array.MainWhiterun_Giver[Index], Array.MainWhiterun_Overview[Index], Array.MainWhiterun_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Whiterun, Array._Array_Toggle_Whiterun, "", Array._Array_Manual_Whiterun)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Falkreath" ;--------------- Falkreath Main Quests		
		Index = 0
		While Index < Array.MainFalkreath_ID.length
			addQuestData(Array.MainFalkreath_Radiant[Index], Array.MainFalkreath_First[Index], Array.MainFalkreath_Final[Index], Array.MainFalkreath_ID[Index], Array.MainFalkreath_Name[Index], Array.MainFalkreath_Giver[Index], Array.MainFalkreath_Overview[Index], Array.MainFalkreath_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Falkreath, Array._Array_Toggle_Falkreath, "", Array._Array_Manual_Falkreath)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Riften" ;--------------- Riften Main Quests		
		Index = 0
		While Index < Array.MainRiften_ID.length
			addQuestData(Array.MainRiften_Radiant[Index], Array.MainRiften_First[Index], Array.MainRiften_Final[Index], Array.MainRiften_ID[Index], Array.MainRiften_Name[Index], Array.MainRiften_Giver[Index], Array.MainRiften_Overview[Index], Array.MainRiften_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Riften, Array._Array_Toggle_Riften, "", Array._Array_Manual_Riften)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Windhelm" ;--------------- Windhelm Main Quests		
		Index = 0
		While Index < Array.MainWindhelm_ID.length
			addQuestData(Array.MainWindhelm_Radiant[Index], Array.MainWindhelm_First[Index], Array.MainWindhelm_Final[Index], Array.MainWindhelm_ID[Index], Array.MainWindhelm_Name[Index], Array.MainWindhelm_Giver[Index], Array.MainWindhelm_Overview[Index], Array.MainWindhelm_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Windhelm, Array._Array_Toggle_Windhelm, "", Array._Array_Manual_Windhelm)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Dawnstar" ;--------------- Dawnstar Main Quests		
		Index = 0
		While Index < Array.MainDawnstar_ID.length
			addQuestData(Array.MainDawnstar_Radiant[Index], Array.MainDawnstar_First[Index], Array.MainDawnstar_Final[Index], Array.MainDawnstar_ID[Index], Array.MainDawnstar_Name[Index], Array.MainDawnstar_Giver[Index], Array.MainDawnstar_Overview[Index], Array.MainDawnstar_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Dawnstar, Array._Array_Toggle_Dawnstar, "", Array._Array_Manual_Dawnstar)
	
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Markarth" ;--------------- Markarth Main Quests		
		Index = 0
		While Index < Array.MainMarkarth_ID.length
			addQuestData(Array.MainMarkarth_Radiant[Index], Array.MainMarkarth_First[Index], Array.MainMarkarth_Final[Index], Array.MainMarkarth_ID[Index], Array.MainMarkarth_Name[Index], Array.MainMarkarth_Giver[Index], Array.MainMarkarth_Overview[Index], Array.MainMarkarth_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Markarth, Array._Array_Toggle_Markarth, "", Array._Array_Manual_Markarth)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Morthal" ;--------------- Morthal Main Quests		
		Index = 0
		While Index < Array.MainMorthal_ID.length
			addQuestData(Array.MainMorthal_Radiant[Index], Array.MainMorthal_First[Index], Array.MainMorthal_Final[Index], Array.MainMorthal_ID[Index], Array.MainMorthal_Name[Index], Array.MainMorthal_Giver[Index], Array.MainMorthal_Overview[Index], Array.MainMorthal_Notes[Index])
			Index += 1
		endWhile

		MCM.questAlloc(Array._Array_Name_Morthal, Array._Array_Toggle_Morthal, "", Array._Array_Manual_Morthal)	
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

	elseif mcmpage == "Solitude" ;--------------- Solitude Main Quests		
		Index = 0
		While Index < Array.MainSolitude_ID.length
			addQuestData(Array.MainSolitude_Radiant[Index], Array.MainSolitude_First[Index], Array.MainSolitude_Final[Index], Array.MainSolitude_ID[Index], Array.MainSolitude_Name[Index], Array.MainSolitude_Giver[Index], Array.MainSolitude_Overview[Index], Array.MainSolitude_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Solitude, Array._Array_Toggle_Solitude, "", Array._Array_Manual_Solitude)	
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Winterhold" ;--------------- Winterhold Main Quests		
		Index = 0
		While Index < Array.MainWinterhold_ID.length
			addQuestData(Array.MainWinterhold_Radiant[Index], Array.MainWinterhold_First[Index], Array.MainWinterhold_Final[Index], Array.MainWinterhold_ID[Index], Array.MainWinterhold_Name[Index], Array.MainWinterhold_Giver[Index], Array.MainWinterhold_Overview[Index], Array.MainWinterhold_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Winterhold, Array._Array_Toggle_Winterhold, "", Array._Array_Manual_Winterhold)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Raven Rock" ;--------------- Raven Rock Quests		
		Index = 0
		While Index < Array.MainRavenRock_ID.length
			addQuestData(Array.MainRavenRock_Radiant[Index], Array.MainRavenRock_First[Index], Array.MainRavenRock_Final[Index], Array.MainRavenRock_ID[Index], Array.MainRavenRock_Name[Index], Array.MainRavenRock_Giver[Index], Array.MainRavenRock_Overview[Index], Array.MainRavenRock_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_RavenRock, Array._Array_Toggle_RavenRock, "Main", Array._Array_Manual_RavenRock)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Skaal Village" ;--------------- Skaal Village Quests		
		Index = 0
		While Index < Array.MainSkaalVillage_ID.length
			addQuestData(Array.MainSkaalVillage_Radiant[Index], Array.MainSkaalVillage_First[Index], Array.MainSkaalVillage_Final[Index], Array.MainSkaalVillage_ID[Index], Array.MainSkaalVillage_Name[Index], Array.MainSkaalVillage_Giver[Index], Array.MainSkaalVillage_Overview[Index], Array.MainSkaalVillage_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_SkaalVillage, Array._Array_Toggle_SkaalVillage, "Main", Array._Array_Manual_SkaalVillage)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Tel Mithryn" ;--------------- Tel Mithryn Quests		
		Index = 0
		While Index < Array.MainTelMithryn_ID.length
			addQuestData(Array.MainTelMithryn_Radiant[Index], Array.MainTelMithryn_First[Index], Array.MainTelMithryn_Final[Index], Array.MainTelMithryn_ID[Index], Array.MainTelMithryn_Name[Index], Array.MainTelMithryn_Giver[Index], Array.MainTelMithryn_Overview[Index], Array.MainTelMithryn_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_TelMithryn, Array._Array_Toggle_TelMithryn, "Main", Array._Array_Manual_TelMithryn)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Thirsk" ;--------------- Thirsk Quests		
		Index = 0
		While Index < Array.MainThirsk_ID.length
			addQuestData(Array.MainThirsk_Radiant[Index], Array.MainThirsk_First[Index], Array.MainThirsk_Final[Index], Array.MainThirsk_ID[Index], Array.MainThirsk_Name[Index], Array.MainThirsk_Giver[Index], Array.MainThirsk_Overview[Index], Array.MainThirsk_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Thirsk, Array._Array_Toggle_Thirsk, "Main", Array._Array_Manual_Thirsk)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Small Towns / Villages" ;--------------- Small Towns Quests		
		Index = 0
		While Index < Array.MainTowns_ID.length
			addQuestData(Array.MainTowns_Radiant[Index], Array.MainTowns_First[Index], Array.MainTowns_Final[Index], Array.MainTowns_ID[Index], Array.MainTowns_Name[Index], Array.MainTowns_Giver[Index], Array.MainTowns_Overview[Index], Array.MainTowns_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Towns, Array._Array_Toggle_Towns, "", Array._Array_Manual_Towns)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Companions" ;--------------- Companions Quests		
		Index = 0
		While Index < Array.MainCompanions_ID.length
			addQuestData(Array.MainCompanions_Radiant[Index], Array.MainCompanions_First[Index], Array.MainCompanions_Final[Index], Array.MainCompanions_ID[Index], Array.MainCompanions_Name[Index], Array.MainCompanions_Giver[Index], Array.MainCompanions_Overview[Index], Array.MainCompanions_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Companions, Array._Array_Toggle_Companions, "", Array._Array_Manual_Companions)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "College of Winterhold" ;--------------- College of Winterhold Quests		
		Index = 0
		While Index < Array.MainCollege_ID.length
			addQuestData(Array.MainCollege_Radiant[Index], Array.MainCollege_First[Index], Array.MainCollege_Final[Index], Array.MainCollege_ID[Index], Array.MainCollege_Name[Index], Array.MainCollege_Giver[Index], Array.MainCollege_Overview[Index], Array.MainCollege_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_College, Array._Array_Toggle_College, "", Array._Array_Manual_College)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Thieves Guild" ;--------------- Theives Guild Quests		
		Index = 0
		While Index < Array.MainRiftenGuild_ID.length
			addQuestData(Array.MainRiftenGuild_Radiant[Index], Array.MainRiftenGuild_First[Index], Array.MainRiftenGuild_Final[Index], Array.MainRiftenGuild_ID[Index], Array.MainRiftenGuild_Name[Index], Array.MainRiftenGuild_Giver[Index], Array.MainRiftenGuild_Overview[Index], Array.MainRiftenGuild_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Thieves, Array._Array_Toggle_Thieves, "", Array._Array_Manual_Thieves)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Dark Brotherhood" ;--------------- Dark Brotherhood Quests		
		Index = 0
		While Index < Array.MainDarkBrotherhood_ID.length
			addQuestData(Array.MainDarkBrotherhood_Radiant[Index], Array.MainDarkBrotherhood_First[Index], Array.MainDarkBrotherhood_Final[Index], Array.MainDarkBrotherhood_ID[Index], Array.MainDarkBrotherhood_Name[Index], Array.MainDarkBrotherhood_Giver[Index], Array.MainDarkBrotherhood_Overview[Index], Array.MainDarkBrotherhood_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Brotherhood, Array._Array_Toggle_Brotherhood, "", Array._Array_Manual_Brotherhood)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Dawnguard" ;--------------- Dawnguard Quests		
		Index = 0
		While Index < Array.MainDawnguard_ID.length
			addQuestData(Array.MainDawnguard_Radiant[Index], Array.MainDawnguard_First[Index], Array.MainDawnguard_Final[Index], Array.MainDawnguard_ID[Index], Array.MainDawnguard_Name[Index], Array.MainDawnguard_Giver[Index], Array.MainDawnguard_Overview[Index], Array.MainDawnguard_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Dawnguard, Array._Array_Toggle_Dawnguard, "", Array._Array_Manual_Dawnguard)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Vampires" ;--------------- Vampires Quests		
		Index = 0
		While Index < Array.MainVampires_ID.length
			addQuestData(Array.MainVampires_Radiant[Index], Array.MainVampires_First[Index], Array.MainVampires_Final[Index], Array.MainVampires_ID[Index], Array.MainVampires_Name[Index], Array.MainVampires_Giver[Index], Array.MainVampires_Overview[Index], Array.MainVampires_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Vampires, Array._Array_Toggle_Vampires, "", Array._Array_Manual_Vampires)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Dungeons" ;--------------- Dungeons Quests		
		Index = 0
		While Index < Array.MainDungeons_ID.length
			addQuestData(Array.MainDungeons_Radiant[Index], Array.MainDungeons_First[Index], Array.MainDungeons_Final[Index], Array.MainDungeons_ID[Index], Array.MainDungeons_Name[Index], Array.MainDungeons_Giver[Index], Array.MainDungeons_Overview[Index], Array.MainDungeons_Notes[Index])
			Index += 1
		endWhile 
		
		MCM.questAlloc(Array._Array_Name_Dungeons, Array._Array_Toggle_Dungeons, "Main", Array._Array_Manual_Dungeons)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Miscellaneous" ;--------------- Misc Quests		
		Index = 0
		While Index < Array.MainMisc_ID.length
			addQuestData(Array.MainMisc_Radiant[Index], Array.MainMisc_First[Index], Array.MainMisc_Final[Index], Array.MainMisc_ID[Index], Array.MainMisc_Name[Index], Array.MainMisc_Giver[Index], Array.MainMisc_Overview[Index], Array.MainMisc_Notes[Index])
			Index += 1
		endWhile 
		
		MCM.questAlloc(Array._Array_Name_Misc, Array._Array_Toggle_Misc, "", Array._Array_Manual_Misc)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Miscellaneous (DG)" ;--------------- Misc Dawnguard Quests		
		Index = 0
		While Index < Array.MiscDG_ID.length
			addQuestData(Array.MiscDG_Radiant[Index], Array.MiscDG_First[Index], Array.MiscDG_Final[Index], Array.MiscDG_ID[Index], Array.MiscDG_Name[Index], Array.MiscDG_Giver[Index], Array.MiscDG_Overview[Index], Array.MiscDG_Notes[Index])
			Index += 1
		endWhile 
	
		MCM.questAlloc(Array._Array_Name_DGMisc, Array._Array_Toggle_DGMisc, "Main", Array._Array_Manual_DGMisc)
	
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Miscellaneous (DB)" ;--------------- Misc Dragonborn Quests		
		Index = 0
		While Index < Array.MiscDB_ID.length
			addQuestData(Array.MiscDB_Radiant[Index], Array.MiscDB_First[Index], Array.MiscDB_Final[Index], Array.MiscDB_ID[Index], Array.MiscDB_Name[Index], Array.MiscDB_Giver[Index], Array.MiscDB_Overview[Index], Array.MiscDB_Notes[Index])
			Index += 1
		endWhile 
		
		MCM.questAlloc(Array._Array_Name_DBMisc, Array._Array_Toggle_DBMisc, "Main", Array._Array_Manual_DBMisc)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Clockwork" ;--------------- Clockwork Quests		
		Index = 0
		While Index < Array.MainClockwork_ID.length
			addQuestData(Array.MainClockwork_Radiant[Index], Array.MainClockwork_First[Index], Array.MainClockwork_Final[Index], Array.MainClockwork_ID[Index], Array.MainClockwork_Name[Index], Array.MainClockwork_Giver[Index], Array.MainClockwork_Overview[Index], Array.MainClockwork_Notes[Index])
			Index += 1
		endWhile 	

		MCM.questAlloc(Array._Array_Name_Clockwork, Array._Array_Toggle_Clockwork, "Main", Array._Array_Manual_Clockwork)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Falskaar" ;--------------- Falskaar Quests		
		Index = 0
		While Index < Array.MainFalskaar_ID.length
			addQuestData(Array.MainFalskaar_Radiant[Index], Array.MainFalskaar_First[Index], Array.MainFalskaar_Final[Index], Array.MainFalskaar_ID[Index], Array.MainFalskaar_Name[Index], Array.MainFalskaar_Giver[Index], Array.MainFalskaar_Overview[Index], Array.MainFalskaar_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Falskaar, Array._Array_Toggle_Falskaar, "Main", Array._Array_Manual_Falskaar)	
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseIf mcmpage == "Helgen Reborn" ;--------------- Helgen Reborn Quests	

		Index = 0
		While Index < 7
			addQuestData(Array.MainHelgenReborn_Radiant[Index], Array.MainHelgenReborn_First[Index], Array.MainHelgenReborn_Final[Index], Array.MainHelgenReborn_ID[Index], Array.MainHelgenReborn_Name[Index], Array.MainHelgenReborn_Giver[Index], Array.MainHelgenReborn_Overview[Index], Array.MainHelgenReborn_Notes[Index])
			Index += 1
		endWhile
		
		If MCM.HR_Faction_Choice == 1
			addQuestData(Array.MainHelgenReborn_Radiant[7],  Array.MainHelgenReborn_First[7],  Array.MainHelgenReborn_Final[7],  Array.MainHelgenReborn_ID[7],  Array.MainHelgenReborn_Name[7],  Array.MainHelgenReborn_Giver[7],  Array.MainHelgenReborn_Overview[7],  Array.MainHelgenReborn_Notes[7])
			addQuestData(Array.MainHelgenReborn_Radiant[8],  Array.MainHelgenReborn_First[8],  Array.MainHelgenReborn_Final[8],  Array.MainHelgenReborn_ID[8],  Array.MainHelgenReborn_Name[8],  Array.MainHelgenReborn_Giver[8],  Array.MainHelgenReborn_Overview[8],  Array.MainHelgenReborn_Notes[8])
			addQuestData(Array.MainHelgenReborn_Radiant[9],  Array.MainHelgenReborn_First[9],  Array.MainHelgenReborn_Final[9],  Array.MainHelgenReborn_ID[9],  Array.MainHelgenReborn_Name[9],  Array.MainHelgenReborn_Giver[9],  Array.MainHelgenReborn_Overview[9],  Array.MainHelgenReborn_Notes[9])
			addQuestData(Array.MainHelgenReborn_Radiant[10], Array.MainHelgenReborn_First[10], Array.MainHelgenReborn_Final[10], Array.MainHelgenReborn_ID[10], Array.MainHelgenReborn_Name[10], Array.MainHelgenReborn_Giver[10], Array.MainHelgenReborn_Overview[10], Array.MainHelgenReborn_Notes[10])
			addQuestData(Array.MainHelgenReborn_Radiant[11], Array.MainHelgenReborn_First[11], Array.MainHelgenReborn_Final[11], Array.MainHelgenReborn_ID[11], Array.MainHelgenReborn_Name[11], Array.MainHelgenReborn_Giver[11], Array.MainHelgenReborn_Overview[11], Array.MainHelgenReborn_Notes[11])
			addQuestData(Array.MainHelgenReborn_Radiant[12], Array.MainHelgenReborn_First[12], Array.MainHelgenReborn_Final[12], Array.MainHelgenReborn_ID[12], Array.MainHelgenReborn_Name[12], Array.MainHelgenReborn_Giver[12], Array.MainHelgenReborn_Overview[12], Array.MainHelgenReborn_Notes[12])
		elseif MCM.HR_Faction_Choice == 2
			addQuestData(Array.MainHelgenReborn_Radiant[13], Array.MainHelgenReborn_First[13], Array.MainHelgenReborn_Final[13], Array.MainHelgenReborn_ID[13], Array.MainHelgenReborn_Name[13], Array.MainHelgenReborn_Giver[13], Array.MainHelgenReborn_Overview[13], Array.MainHelgenReborn_Notes[13])
			addQuestData(Array.MainHelgenReborn_Radiant[14], Array.MainHelgenReborn_First[14], Array.MainHelgenReborn_Final[14], Array.MainHelgenReborn_ID[14], Array.MainHelgenReborn_Name[14], Array.MainHelgenReborn_Giver[14], Array.MainHelgenReborn_Overview[14], Array.MainHelgenReborn_Notes[14])
			addQuestData(Array.MainHelgenReborn_Radiant[15], Array.MainHelgenReborn_First[15], Array.MainHelgenReborn_Final[15], Array.MainHelgenReborn_ID[15], Array.MainHelgenReborn_Name[15], Array.MainHelgenReborn_Giver[15], Array.MainHelgenReborn_Overview[15], Array.MainHelgenReborn_Notes[15])
			addQuestData(Array.MainHelgenReborn_Radiant[16], Array.MainHelgenReborn_First[16], Array.MainHelgenReborn_Final[16], Array.MainHelgenReborn_ID[16], Array.MainHelgenReborn_Name[16], Array.MainHelgenReborn_Giver[16], Array.MainHelgenReborn_Overview[16], Array.MainHelgenReborn_Notes[16])
			addQuestData(Array.MainHelgenReborn_Radiant[17], Array.MainHelgenReborn_First[17], Array.MainHelgenReborn_Final[17], Array.MainHelgenReborn_ID[17], Array.MainHelgenReborn_Name[17], Array.MainHelgenReborn_Giver[17], Array.MainHelgenReborn_Overview[17], Array.MainHelgenReborn_Notes[17])
			addQuestData(Array.MainHelgenReborn_Radiant[18], Array.MainHelgenReborn_First[18], Array.MainHelgenReborn_Final[18], Array.MainHelgenReborn_ID[18], Array.MainHelgenReborn_Name[18], Array.MainHelgenReborn_Giver[18], Array.MainHelgenReborn_Overview[18], Array.MainHelgenReborn_Notes[18])
		elseif MCM.HR_Faction_Choice == 3
			addQuestData(Array.MainHelgenReborn_Radiant[19], Array.MainHelgenReborn_First[19], Array.MainHelgenReborn_Final[19], Array.MainHelgenReborn_ID[19], Array.MainHelgenReborn_Name[19], Array.MainHelgenReborn_Giver[19], Array.MainHelgenReborn_Overview[19], Array.MainHelgenReborn_Notes[19])
			addQuestData(Array.MainHelgenReborn_Radiant[20], Array.MainHelgenReborn_First[20], Array.MainHelgenReborn_Final[20], Array.MainHelgenReborn_ID[20], Array.MainHelgenReborn_Name[20], Array.MainHelgenReborn_Giver[20], Array.MainHelgenReborn_Overview[20], Array.MainHelgenReborn_Notes[20])
			addQuestData(Array.MainHelgenReborn_Radiant[21], Array.MainHelgenReborn_First[21], Array.MainHelgenReborn_Final[21], Array.MainHelgenReborn_ID[21], Array.MainHelgenReborn_Name[21], Array.MainHelgenReborn_Giver[21], Array.MainHelgenReborn_Overview[21], Array.MainHelgenReborn_Notes[21])
			addQuestData(Array.MainHelgenReborn_Radiant[22], Array.MainHelgenReborn_First[22], Array.MainHelgenReborn_Final[22], Array.MainHelgenReborn_ID[22], Array.MainHelgenReborn_Name[22], Array.MainHelgenReborn_Giver[22], Array.MainHelgenReborn_Overview[22], Array.MainHelgenReborn_Notes[22])
			addQuestData(Array.MainHelgenReborn_Radiant[23], Array.MainHelgenReborn_First[23], Array.MainHelgenReborn_Final[23], Array.MainHelgenReborn_ID[23], Array.MainHelgenReborn_Name[23], Array.MainHelgenReborn_Giver[23], Array.MainHelgenReborn_Overview[23], Array.MainHelgenReborn_Notes[23])
			addQuestData(Array.MainHelgenReborn_Radiant[24], Array.MainHelgenReborn_First[24], Array.MainHelgenReborn_Final[24], Array.MainHelgenReborn_ID[24], Array.MainHelgenReborn_Name[24], Array.MainHelgenReborn_Giver[24], Array.MainHelgenReborn_Overview[24], Array.MainHelgenReborn_Notes[24])
		elseif MCM.HR_Faction_Choice == 0
			addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
			addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
			addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
			addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
			addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")
			addQuestData(false, -999, 999, "Completionist_MQ", "Configure From Settings Page", "", "Please choose the faction you are playing from the Menu in the settings page to show the correct quests", "")			
		endIf
		
		MCM.questAlloc(Array._Array_Name_HelgenReborn, Array._Array_Toggle_HelgenReborn, "Main", Array._Array_Manual_HelgenReborn)	
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Moonpath To Elsweyr" ;--------------- Moonpath to Elsweyr Quests		
		Index = 0
		While Index < Array.MainMoonpath_ID.length
			addQuestData(Array.MainMoonpath_Radiant[Index], Array.MainMoonpath_First[Index], Array.MainMoonpath_Final[Index], Array.MainMoonpath_ID[Index], Array.MainMoonpath_Name[Index], Array.MainMoonpath_Giver[Index], Array.MainMoonpath_Overview[Index], Array.MainMoonpath_Notes[Index])
			Index += 1
		endWhile

		MCM.questAlloc(Array._Array_Name_Moonpath, Array._Array_Toggle_Moonpath, "Main", Array._Array_Manual_Moonpath)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Moon And Star" ;--------------- Moon and Star Quests		
		Index = 0
		While Index < Array.MainMoonStar_ID.length
			addQuestData(Array.MainMoonStar_Radiant[Index], Array.MainMoonStar_First[Index], Array.MainMoonStar_Final[Index], Array.MainMoonStar_ID[Index], Array.MainMoonStar_Name[Index], Array.MainMoonStar_Giver[Index], Array.MainMoonStar_Overview[Index], Array.MainMoonStar_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_MoonStar, Array._Array_Toggle_MoonStar, "Main", Array._Array_Manual_MoonStar)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Project AHO" ;--------------- Project AHO Quests		
		Index = 0
		While Index < Array.MainProjectAHO_ID.length
			addQuestData(Array.MainProjectAHO_Radiant[Index], Array.MainProjectAHO_First[Index], Array.MainProjectAHO_Final[Index], Array.MainProjectAHO_ID[Index], Array.MainProjectAHO_Name[Index], Array.MainProjectAHO_Giver[Index], Array.MainProjectAHO_Overview[Index], Array.MainProjectAHO_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_ProjectAHO, Array._Array_Toggle_ProjectAHO, "Main", Array._Array_Manual_ProjectAHO)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Skyrim Underground" ;--------------- Skyrim Underground Quests		
		Index = 0
		While Index < Array.MainUnderground_ID.length
			addQuestData(Array.MainUnderground_Radiant[Index], Array.MainUnderground_First[Index], Array.MainUnderground_Final[Index], Array.MainUnderground_ID[Index], Array.MainUnderground_Name[Index], Array.MainUnderground_Giver[Index], Array.MainUnderground_Overview[Index], Array.MainUnderground_Notes[Index])
			Index += 1
		endWhile	
		
		MCM.questAlloc(Array._Array_Name_Underground, Array._Array_Toggle_Underground, "Main", Array._Array_Manual_Underground)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
	elseif mcmpage == "The Wheels Of Lull" ;--------------- The Wheels of Lull Quests		
		Index = 0
		While Index < Array.MainWheelsofLull_ID.length
			addQuestData(Array.MainWheelsofLull_Radiant[Index], Array.MainWheelsofLull_First[Index], Array.MainWheelsofLull_Final[Index], Array.MainWheelsofLull_ID[Index], Array.MainWheelsofLull_Name[Index], Array.MainWheelsofLull_Giver[Index], Array.MainWheelsofLull_Overview[Index], Array.MainWheelsofLull_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_WheelsofLull, Array._Array_Toggle_WheelsofLull, "", Array._Array_Manual_WheelsofLull)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "The Gray Cowl Of Nocturnal" ;--------------- The Gray Cowl of Nocturnal Quests		
		Index = 0
		While Index < Array.MainGrayCowl_ID.length
			addQuestData(Array.MainGrayCowl_Radiant[Index], Array.MainGrayCowl_First[Index], Array.MainGrayCowl_Final[Index], Array.MainGrayCowl_ID[Index], Array.MainGrayCowl_Name[Index], Array.MainGrayCowl_Giver[Index], Array.MainGrayCowl_Overview[Index], Array.MainGrayCowl_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_GrayCowl, Array._Array_Toggle_GrayCowl, "Main", Array._Array_Manual_GrayCowl)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Undeath" ;--------------- Undeath Quests		
		Index = 0
		While Index < Array.MainUndeath_ID.length
			addQuestData(Array.MainUndeath_Radiant[Index], Array.MainUndeath_First[Index], Array.MainUndeath_Final[Index], Array.MainUndeath_ID[Index], Array.MainUndeath_Name[Index], Array.MainUndeath_Giver[Index], Array.MainUndeath_Overview[Index], Array.MainUndeath_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Undeath, Array._Array_Toggle_Undeath, "Main", Array._Array_Manual_Undeath)	
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Wyrmstooth" ;--------------- Wyrmstooth Quests		
		Index = 0
		While Index < Array.MainWyrmstooth_ID.length
			addQuestData(Array.MainWyrmstooth_Radiant[Index], Array.MainWyrmstooth_First[Index], Array.MainWyrmstooth_Final[Index], Array.MainWyrmstooth_ID[Index], Array.MainWyrmstooth_Name[Index], Array.MainWyrmstooth_Giver[Index], Array.MainWyrmstooth_Overview[Index], Array.MainWyrmstooth_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_Wyrmstooth, Array._Array_Toggle_Wyrmstooth, "Main", Array._Array_Manual_Wyrmstooth)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "3DNPC Main Quests" ;--------------- 3DNPC Main Quests		
		Index = 0
		While Index < Array.Main3DNPC_Main_ID.length
			addQuestData(Array.Main3DNPC_Main_Radiant[Index], Array.Main3DNPC_Main_First[Index], Array.Main3DNPC_Main_Final[Index], Array.Main3DNPC_Main_ID[Index], Array.Main3DNPC_Main_Name[Index], Array.Main3DNPC_Main_Giver[Index], Array.Main3DNPC_Main_Overview[Index], Array.Main3DNPC_Main_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_3DNPC_Main, Array._Array_Toggle_3DNPC_Main, "Main", Array._Array_Manual_3DNPC_Main)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "3DNPC Blood of Kings" ;--------------- 3DNPC Blood of Kings Quests		
		Index = 0
		While Index < Array.Main3DNPC_BoK_ID.length
			addQuestData(Array.Main3DNPC_BoK_Radiant[Index], Array.Main3DNPC_BoK_First[Index], Array.Main3DNPC_BoK_Final[Index], Array.Main3DNPC_BoK_ID[Index], Array.Main3DNPC_BoK_Name[Index], Array.Main3DNPC_BoK_Giver[Index], Array.Main3DNPC_BoK_Overview[Index], Array.Main3DNPC_BoK_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_3DNPC_BOK, Array._Array_Toggle_3DNPC_BOK, "Main", Array._Array_Manual_3DNPC_BOK)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "3DNPC Darkened Steel" ;--------------- 3DNPC Darkened Steel Quests		
		Index = 0
		While Index < Array.Main3DNPC_DS_ID.length
			addQuestData(Array.Main3DNPC_DS_Radiant[Index], Array.Main3DNPC_DS_First[Index], Array.Main3DNPC_DS_Final[Index], Array.Main3DNPC_DS_ID[Index], Array.Main3DNPC_DS_Name[Index], Array.Main3DNPC_DS_Giver[Index], Array.Main3DNPC_DS_Overview[Index], Array.Main3DNPC_DS_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_3DNPC_DS, Array._Array_Toggle_3DNPC_DS, "Main", Array._Array_Manual_3DNPC_DS)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "3DNPC Misc Quests" ;--------------- 3DNPC Misc Quests		
		Index = 0
		While Index < Array.Main3DNPC_Misc_ID.length
			addQuestData(Array.Main3DNPC_Misc_Radiant[Index], Array.Main3DNPC_Misc_First[Index], Array.Main3DNPC_Misc_Final[Index], Array.Main3DNPC_Misc_ID[Index], Array.Main3DNPC_Misc_Name[Index], Array.Main3DNPC_Misc_Giver[Index], Array.Main3DNPC_Misc_Overview[Index], Array.Main3DNPC_Misc_Notes[Index])
			Index += 1
		endWhile
		
		MCM.questAlloc(Array._Array_Name_3DNPC_Misc, Array._Array_Toggle_3DNPC_Misc, "Main", Array._Array_Manual_3DNPC_Misc)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	elseif mcmpage == "Vigilant Main Quests" ;--------------- Vigilant Main Quests		
		Index = 0
		While Index < Array.VigilantMain_ID.length
			addQuestData(Array.VigilantMain_Radiant[Index], Array.VigilantMain_First[Index], Array.VigilantMain_Final[Index], Array.VigilantMain_ID[Index], Array.VigilantMain_Name[Index], Array.VigilantMain_Giver[Index], Array.VigilantMain_Overview[Index], Array.VigilantMain_Notes[Index])
			Index += 1
		endWhile	
		
		MCM.questAlloc(Array._Array_Name_VigilantActs, Array._Array_Toggle_VigilantActs, "Main", Array._Array_Manual_VigilantActs)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Vigilant Side Quests" ;--------------- Vigilant Side Quests		
		Index = 0
		While Index < Array.VigilantSide_ID.length
			addQuestData(Array.VigilantSide_Radiant[Index], Array.VigilantSide_First[Index], Array.VigilantSide_Final[Index], Array.VigilantSide_ID[Index], Array.VigilantSide_Name[Index], Array.VigilantSide_Giver[Index], Array.VigilantSide_Overview[Index], Array.VigilantSide_Notes[Index])
			Index += 1
		endWhile	
		
		MCM.questAlloc(Array._Array_Name_VigilantSide, Array._Array_Toggle_VigilantSide, "", Array._Array_Manual_VigilantSide)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Vigilant Memory Quests" ;--------------- Vigilant Memory Quests		
		Index = 0
		While Index < Array.VigilantMem_ID.length
			addQuestData(Array.VigilantMem_Radiant[Index], Array.VigilantMem_First[Index], Array.VigilantMem_Final[Index], Array.VigilantMem_ID[Index], Array.VigilantMem_Name[Index], Array.VigilantMem_Giver[Index], Array.VigilantMem_Overview[Index], Array.VigilantMem_Notes[Index])
			Index += 1
		endWhile	
		
		MCM.questAlloc(Array._Array_Name_VigilantMem, Array._Array_Toggle_VigilantMem, "Main", Array._Array_Manual_VigilantMem)
		
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
;;---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	elseif mcmpage == "Vigilant Radiant Quests" ;--------------- Vigilant Radiant Quests		
		Index = 0
		While Index < Array.VigilantRadiant_ID.length
			addQuestData(Array.VigilantRadiant_Radiant[Index], Array.VigilantRadiant_First[Index], Array.VigilantRadiant_Final[Index], Array.VigilantRadiant_ID[Index], Array.VigilantRadiant_Name[Index], Array.VigilantRadiant_Giver[Index], Array.VigilantRadiant_Overview[Index], Array.VigilantRadiant_Notes[Index])
			Index += 1
		endWhile

		MCM.questAlloc(Array._Array_Name_VigilantRadiant, Array._Array_Toggle_VigilantRadiant, "Radiant", Array._Array_Manual_VigilantRadiant)
	endIf	
endFunction
